import { StyleSheet, View, Text } from "react-native";
import React, { useState, useEffect } from "react";
import MapView from "react-native-maps";
import * as Location from "expo-location";

export default function MapME() {
  const [location, setLocation] = useState(null);
  const [errorMsg, setErrorMsg] = useState(null);


  useEffect(() => {
    (async () => {
      let { status } = await Location.requestForegroundPermissionsAsync();
      if (status !== "granted") {
        setErrorMsg("Permission to access location was denied");
        return;
      }

      let location = await Location.getCurrentPositionAsync({});
      setLocation(location);
      console.log(location.coords)
    })();
  }, []);

  return (
    <>
        <View style={styles.mapCon}>
          <MapView
            style={styles.map}
            initialRegion={{
              latitude: location? location.coords.latitude : 37.28825,
              longitude: location? location.coords.longitude: -122.4324 ,
              latitudeDelta: 0,
              longitudeDelta: 0,
            }}
            zoomEnabled
            zoomControlEnabled
            mapType="standard"
          />
        </View>
    </>
  );
}

const styles = StyleSheet.create({
  mapCon: {
    flex: 1,
    backgroundColor: "red",
  },
  map: {
    flex: 1,
  },
});
