// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getAuth } from "firebase/auth";

import { getFirestore } from "firebase/firestore";

const firebaseConfig = {
  apiKey: "AIzaSyAkJaLtqG7a0zQIhyR_aijroK7Piu6gt2E",
  authDomain: "erozgar5-fc889.firebaseapp.com",
  projectId: "erozgar5-fc889",
  storageBucket: "erozgar5-fc889.appspot.com",
  messagingSenderId: "929497575378",
  appId: "1:929497575378:web:6cc0cd37f3ace4671e220b",
  measurementId: "G-Q8LYBKV9H5",
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const auth = getAuth(app);

const firestore = getFirestore(app); // this will attafch my keys with my firestore

export { auth, firestore };
