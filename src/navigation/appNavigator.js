import { NavigationContainer } from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";

const MainStack = createNativeStackNavigator();
import Home from "../screens/home";
import Profile from "../screens/profile";
import Details from "../screens/details";
import MapME from "../screens/mapMe";
import Users from "../screens/users";
import Login from "../screens/login";
import SignupWithFirebase from "../screens/signupWithFirebase";

function AppNavigator() {
  return (
    <NavigationContainer>
      <MainStack.Navigator>
        <MainStack.Screen
          name={"SignupWithFirebase"}
          component={SignupWithFirebase}
          options={{
            headerShown: false,
          }}
        />
        <MainStack.Screen name={"MapMe"} component={MapME} />
        <MainStack.Screen
          name={"Login"}
          component={Login}
          options={{
            headerShown: false,
          }}
        />

        <MainStack.Screen
          name={"Users"}
          component={Users}
          options={{
            headerShown: false,
          }}
        />
        <MainStack.Screen
          name={"Profile"}
          component={Profile}
          options={{
            headerShown: false,
          }}
        />
        <MainStack.Screen
          name={"Home"}
          component={Home}
          options={{
            headerTitle: "Custom Header", // Set a custom header title
            headerStyle: { backgroundColor: "red" }, // Customize the header background color
            headerTintColor: "white", // Customize the header text color
          }}
        />
        <MainStack.Screen name={"Details"} component={Details} />
      </MainStack.Navigator>
    </NavigationContainer>
  );
}

export { AppNavigator };
