import {
  StyleSheet,
  Text,
  Touchable,
  TouchableOpacity,
  View,
} from "react-native";
import React from "react";

function CustomButton({ text, bgColor, textColor, height, width, onBtnPress }) {
  return (
    <TouchableOpacity
      onPress={onBtnPress}
      style={{
        backgroundColor: bgColor,
        height: height,
        width: width,
        padding: 10,
        margin: 10,
        borderRadius: 10,
        alignItems: "center",
      }}
    >
      <Text style={{ color: textColor }}>{text}</Text>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({});

export { CustomButton };
